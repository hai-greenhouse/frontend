import sqlite3
from clog import clog

class vDB():

    def __init__(self):
        self._con = False
        self._dbh = False
        self.connect()

    def connect(self):
        try:
            self._dbh = sqlite3.connect('/var/www/html/c-hAI/data.db', check_same_thread=False)
            self._con = True

        except Exception as e:
            self_con = False
            clog( "[Error] [vDB] connect: error!! ", str(e) )

        return self._con

    def getLightNow(self):
        try:
            self.connect()
            sql = "select `light` from Light where 1=1 Order by `currentdate` DESC limit 1"
            self.cur = self._dbh.cursor()
            self.cur.execute( sql )
            r = self.cur.fetchone()
            return int(r[0])
        except Exception as e:
            clog( "[Error] vDB:getLightNow,sql:", sql )

        return 0

    def getLight(self, stime = '1911-01-01 00:00:00', etime = '9999-99-99 23:59:59' ):
        try:
            self.connect()
            sql = "select `light`,`currentdate` from Light where currentdate > '%s' and currentdate < '%s'" % ( stime, etime )
            self.cur = self._dbh.cursor()
            self.cur.execute( sql )
            return self.cur.fetchall()
        except Exception as e:
            clog( "[Error] vDB:getLight:stime,etime:", stime, " - ", etime )

        return (0, 0)
    
    def getLightOrder(self, stime = '1911-01-01 00:00:00', etime = '9999-99-99 23:59:59', order = 'Desc' ):
        try:
            self.connect()
            sql = "select `light`,`currentdate` from Light where currentdate > '%s' and currentdate < '%s' order by currentdate %s" % ( stime, etime, order )
            self.cur = self._dbh.cursor()
            self.cur.execute( sql )
            return self.cur.fetchall()
        except Exception as e:
            clog( "[Error] vDB:getLightOrder:stime,etime:", stime, " - ", etime )

        return (0, 0)

    def getTemp(self, stime = '1911-01-01 00:00:00', etime = '9999-99-99 23:59:59' ):
        try:
            self.connect()
            sql = "select `temperature`,`currentdate` from Temperature where currentdate > '%s' and currentdate < '%s' order by `currentdate` DESC" % ( stime, etime )
            self.cur = self._dbh.cursor()
            self.cur.execute( sql )
            return self.cur.fetchall()
        except Exception as e:
            clog( "[Error] vDB:getTemp:stime,etime:", stime, " - ", etime )

        return (0, 0)

    def getTempNow(self):
        return self.getTemp()[0]

    def getHumi(self, stime = '1911-01-01 00:00:00', etime = '9999-99-99 23:59:59' ):
        try:
            self.connect()
            sql = "select `humidity`,`currentdate` from Humidity where currentdate > '%s' and currentdate < '%s' order by `currentdate` DESC" % ( stime, etime )
            self.cur = self._dbh.cursor()
            self.cur.execute( sql )
            return self.cur.fetchall()
        except Exception as e:
            clog( "[Error] vDB:getHumi:stime,etime:", stime, " - ", etime )

        return (0, 0)

    def getHumiNow(self):
        return self.getHumi()[0]

    def getLightMin(self, stime = '1911-01-01 00:00:00', etime = '9999-99-99 23:59:59' ):
        try:
            self.connect()
            sql = "select IFNULL( min(`light`), 0) from Light where currentdate > '%s' and currentdate < '%s'" % ( stime, etime )
            self.cur = self._dbh.cursor()
            self.cur.execute( sql )
            row = self.cur.fetchone()
        except Exception as e:
            clog( "[Error] vDB:getLightMIN:stime,etime:", stime, " - ", etime )
            return 0

        return row[0]

    def getLightMax(self, stime = '1911-01-01 00:00:00', etime = '9999-99-99 23:59:59' ):
        try:
            self.connect()
            sql = "select IFNULL( MAX(`light`), 0) from Light where currentdate > '%s' and currentdate < '%s'" % ( stime, etime )
            self.cur = self._dbh.cursor()
            self.cur.execute( sql )
            row = self.cur.fetchone()
        except Exception as e:
            clog( "[Error] vDB:getLightMAX:stime,etime:", stime, " - ", etime )
            return 0

        return row[0]


    def getLightAvg(self, stime = '1911-01-01 00:00:00', etime = '9999-99-99 23:59:59' ):
        try:
            self.connect()
            sql = "select IFNULL( avg(`light`), 0) from Light where currentdate > '%s' and currentdate < '%s'" % ( stime, etime )
            self.cur = self._dbh.cursor()
            self.cur.execute( sql )
            row = self.cur.fetchone()
        except Exception as e:
            clog( "[Error] vDB:getLightAvg:stime,etime:", stime, " - ", etime )
            return 0

        return row[0]

    def getTempAvg(self, stime = '1911-01-01 00:00:00', etime = '9999-99-99 23:59:59' ):
        try:
            self.connect()
            sql = "select IFNULL( avg(`temperature`), 0) from Temperature where currentdate > '%s' and currentdate < '%s'" % ( stime, etime )
            self.cur = self._dbh.cursor()
            self.cur.execute( sql )
            row = self.cur.fetchone()
        except Exception as e:
            clog( "[Error] vDB:getTempAvg:stime,etime:", stime, " - ", etime )
            return 0

        return row[0]

    def getHumiAvg(self, stime = '1911-01-01 00:00:00', etime = '9999-99-99 23:59:59' ):
        try:
            self.connect()
            sql = "select IFNULL( avg(`humidity`), 0) from `Humidity` where currentdate > '%s' and currentdate < '%s'" % ( stime, etime )
            self.cur = self._dbh.cursor()
            self.cur.execute( sql )
            row = self.cur.fetchone()
        except Exception as e:
            clog( "[Error] vDB:getHumiAvg:stime,etime:", stime, " - ", etime, " e:", str(e) )
            return 0

        return row[0]

    def getPOnOff(self, stime = '1911-01-01 00:00:00', etime = '9999-99-99 23:59:59' ):
        try:
            result = {}
            self.connect()
            sql = "select `Item` from `POnOff` where `pStatus`='ON' and currentdate > '%s' and currentdate < '%s'" % ( stime, etime )
            self.cur = self._dbh.cursor()
            self.cur.execute( sql )
            row = self.cur.fetchall()
            for r in ( row ):
                result[ r[0] ] = "ON"
        except Exception as e:
            clog( "[Error] vDB:getPOnOff:stime,etime:", stime, " - ", etime )
            return 0

        return result

    def getLFHPStatus(self):
        try:
            self.connect()
            sql = "select `LightOff`,`LightOn` from TimeOnOff where `Hour24H` = %d" % hour
            self.cur = self._dbh.cursor()
            self.cur.execute( sql )
            (LightOff, LightOn) = self.cur.fetchone()
        except Exception as e:
            clog( "[Error] vDB:getLightOnOff," + e )
            return(-9999, -999 )

        return (LightOff, LightOn)




    ##########################################################################################


    def getLightOnOff(self, hour):
        try:
            self.connect()
            sql = "select `LightOff`,`LightOn` from TimeOnOff where `Hour24H` = %d" % hour
            self.cur = self._dbh.cursor()
            self.cur.execute( sql )
            (LightOff, LightOn) = self.cur.fetchone()
        except Exception as e:
            clog( "[Error] vDB:getLightOnOff," + e )
            return(-9999, -999 )

        return (LightOff, LightOn)

    def getTempOn(self, hour):
        try:
            self.connect()
            sql = "select `TempOn` from TimeOnOff where `Hour24H` = %d" % hour
            self.cur = self._dbh.cursor()
            self.cur.execute( sql )
            TempOn = self.cur.fetchone()
        except Exception as e:
            clog( "[Error] vDB:getTempOn," + e )
            return -999

        return TempOn[0]

    def getHumiOn(self, hour):
        try:
            self.connect()
            sql = "select `HumiOn` from TimeOnOff where `Hour24H` = %d" % hour
            self.cur = self._dbh.cursor()
            self.cur.execute( sql )
            HumiOn = self.cur.fetchone()
        except Exception as e:
            clog( "[Error] vDB:getHumiOn," + e )
            return -999

        return HumiOn[0]

    def getDoorOnOff(self, hour):
        try:
            self.connect()
            sql = "select `DoorOnOff` from TimeOnOff where `Hour24H` = %d" % hour
            self.cur = self._dbh.cursor()
            self.cur.execute( sql )
            DoorOnOff = self.cur.fetchone()
        except Exception as e:
            clog( "[Error] vDB:getHumiOn," + e )
            return 0

        return DoorOnOff[0]

    def getInterval(self, item):
        try:
            self.connect()
            sql = "select `seconds` from Interval where `item` = '%s'" % item
            self.cur = self._dbh.cursor()
            self.cur.execute( sql )
            seconds = self.cur.fetchone()
        except Exception as e:
            clog( "[Error] vDB:getInterval," + str(e) )
            return 900
        
        return seconds[0]

    def getSettings(self, item):
        try:
            self.connect()
            sql = "select `setting` from `Settings` where `item` = '%s'" % item
            self.cur = self._dbh.cursor()
            self.cur.execute( sql )
            setting = self.cur.fetchone()
        except Exception as e:
            clog( "[Error] vDB:getInterval," + str(e) )
            return False
        
        return setting[0]

