from flask import Flask
#from flask_sqlalchemy import SQLAlchemy

from sqlalchemy import Column, Integer, String, Float, DateTime, create_engine, func, sql
#engine = create_engine('sqlite:///sales.db', echo = True)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

Base = declarative_base()

class vDB():

	#_engine = create_engine('sqlite+pysqlite:///:memory:', echo = True )
	_engine = create_engine('sqlite:////home/hAI/data.db?check_same_thread=False', echo = False )

	def __init__(self):
		Base.metadata.create_all(self._engine)
		self.getSession()
		# https://www.tutorialspoint.com/sqlalchemy/sqlalchemy_orm_declaring_mapping.htm
		
	def getSession(self):
		self._Session = sessionmaker( bind = self._engine )
		self._session = self._Session() 
		return self._session
		#return Session(self._engine)

	def getIntervals(self):
		result = self._session.query( Interval ).order_by( Interval._id ).all()
		return result

	def getTimeOnOffTable(self):
		result = self._session.query( TimeOnOff ).order_by( TimeOnOff._id ).all()
		return result

	def getLightNow(self):
		result = self._session.query(Light._light).order_by( Light._curdate.desc() ).first()
		if result is None:
			return 0

		return result._light

	def getLight(self, stime = '1911-01-01 00:00:00', etime = '9999-99-99 23:59:59' ):
		result = self._session.query(Light).filter( Light._curdate > stime, Light._curdate < etime)
		if result is None:
			return (0,0)

		return result

			#sql = "select `light`,`currentdate` from Light where currentdate > '%s' and currentdate < '%s'" % ( stime, etime )
			#return self.cur.fetchall()
	
	def getLightOrder(self, stime = '1911-01-01 00:00:00', etime = '9999-99-99 23:59:59', order = 'Desc' ):
		if order == "Desc":
			result = self._session.query( Light._light, Light._curdate ).filter( Light._curdate > stime, Light._curdate < etime).order_by( Light._curdate.desc() ).all()
		else:
			result = self._session.query( Light._light, Light._curdate ).filter( Light._curdate > stime, Light._curdate < etime).order_by( Light._curdate.asc() ).all()


		if result is None:
			return (0, 0)

		return result

	def getLightMin(self, stime = '1911-01-01 00:00:00', etime = '9999-99-99 23:59:59' ):
		#sql = "select IFNULL( min(`light`), 0) from Light where currentdate > '%s' and currentdate < '%s'" % ( stime, etime )
		#row = self.cur.fetchone()
		result = self._session.query( func.ifnull( func.min( Light._light ), 0 ) ).filter( Light._curdate > stime, Light._curdate < etime ).one() 

		if result is None:
			return 0

		return result

	def getLightMax(self, stime = '1911-01-01 00:00:00', etime = '9999-99-99 23:59:59' ):
		result = self._session.query( func.ifnull( func.max( Light._light ), 0 ) ).filter( Light._curdate > stime, Light._curdate < etime ).one()
		#sql = "select IFNULL( MAX(`light`), 0) from Light where currentdate > '%s' and currentdate < '%s'" % ( stime, etime )
		#row = self.cur.fetchone()

		if result is None:
			return 0

		return result


	def getLightAvg(self, stime = '1911-01-01 00:00:00', etime = '9999-99-99 23:59:59' ):
		result = self._session.query( func.ifnull( func.avg( Light._light ), 0 ) ).filter( Light._curdate > stime, Light._curdate < etime ).one()
		#sql = "select IFNULL( avg(`light`), 0) from Light where currentdate > '%s' and currentdate < '%s'" % ( stime, etime )
		#row = self.cur.fetchone()

		if result is None:
			return 0

		return result

	def getTemp(self, stime = '1911-01-01 00:00:00', etime = '9999-99-99 23:59:59' ):
		result = self._session.query( Temp ).filter( Temp._curdate > stime, Temp._curdate < etime ).order_by( Temp._curdate.desc() )

			#sql = "select `temperature`,`currentdate` from Temperature where currentdate > '%s' and currentdate < '%s' order by `currentdate` DESC" % ( stime, etime )
			#return self.cur.fetchall()

		if result is None:
			return (0, 0)

		return result

	def getTempNow(self):
		return self.getTemp()[0]

	def getTempAvg(self, stime = '1911-01-01 00:00:00', etime = '9999-99-99 23:59:59' ):
		result = self._session.query( func.ifnull( func.avg( Temp._temp ), 0 ) ).filter( Temp._curdate > stime, Temp._curdate < etime ).one()

			#sql = "select IFNULL( avg(`temperature`), 0) from Temperature where currentdate > '%s' and currentdate < '%s'" % ( stime, etime )
			#row = self.cur.fetchone()

		if result is None:
			return 0

		return result

	def getHumi(self, stime = '1911-01-01 00:00:00', etime = '9999-99-99 23:59:59' ):
		result = self._session.query(Humi._humi, Humi._curdate).filter( Humi._curdate > stime, Humi._curdate < etime ).order_by( Humi._curdate.desc() )

			#sql = "select `humidity`,`currentdate` from Humidity where currentdate > '%s' and currentdate < '%s' order by `currentdate` DESC" % ( stime, etime )
			#return self.cur.fetchall()
		if result is None:
			return (0, 0)

		return result


	def getHumiNow(self):
		return self.getHumi()[0]

	def getHumiAvg(self, stime = '1911-01-01 00:00:00', etime = '9999-99-99 23:59:59' ):
		result = self._session.query( func.ifnull( func.avg( Humi._humi ), 0 ) ).filter( Humi._curdate > stime, Humi._curdate < etime ).one()
			#sql = "select IFNULL( avg(`humidity`), 0) from `Humidity` where currentdate > '%s' and currentdate < '%s'" % ( stime, etime )
			#row = self.cur.fetchone()

		if result is None:
			return 0

		return result


	##########################################################################################


	def getLightOnOff(self, hour):
		result = self._session.query( TimeOnOff._lightoff, TimeOnOff.lighton ).filter( TimeOnOff._hour24 == hour ).one()
			#sql = "select `LightOff`,`LightOn` from TimeOnOff where `Hour24H` = %d" % hour
			#(LightOff, LightOn) = self.cur.fetchone()
		if result is None:
			return(-9999, -999 )

		return ( result._lightoff, result._lighton )

	def getTempOn(self, hour):
		result = self._session.query( TimeOnOff._tempon ).filter( TimeOnOff._hour24 == hour ).one()

		if result is None:
			return -999

		return result[0]
			#sql = "select `TempOn` from TimeOnOff where `Hour24H` = %d" % hour
			#TempOn = self.cur.fetchone()

	def getHumiOn(self, hour):
		result = self._session.query( TimeOnOff._humion ).filter( TimeOnOff._hour24 == hour ).one()
		#sql = "select `HumiOn` from TimeOnOff where `Hour24H` = %d" % hour
		#HumiOn = self.cur.fetchone()

		if result is None:
			return -999

		return result[0]

	def getDoorOnOff(self, hour):
		result = self._session.query( TimeOnOff._dooronoff ).filter( TimeOnOff._hour24 == hour ).one()
		#sql = "select `DoorOnOff` from TimeOnOff where `Hour24H` = %d" % hour
		#DoorOnOff = self.cur.fetchone()

		if result is None:
			return 0

		return result[0]

	def getInterval(self, item):
		result = self._session.query( Interval._seconds ).filter( Interval._item == item ).one()
		#sql = "select `seconds` from Interval where `item` = '%s'" % item
		#seconds = self.cur.fetchone()
		if result is None:
			return 900
		
		return result._seconds

	def getSettings(self, item):
		result = self._session.query( Settings._setting ).filter( Settings._item == item ).one()

		return result[0]
			#sql = "select `setting` from `Settings` where `item` = '%s'" % item
			#setting = self.cur.fetchone()

class TimeOnOff(Base):

# CREATE TABLE TimeOnOff(id INTEGER PRIMARY KEY AUTOINCREMENT, Hour24H INTEGER, LightOff INTEGER, LightOn INTEGER, TempOff INTEGER, TempOn INTEGER, HumiOff INTEGER, HumiOn INTEGER, UUID TEXT, `DoorOnOff` INTEGER, LightMode integer, TempMode integer, HumiMode integer);
	__tablename__ = 'TimeOnOff'
	_id = Column('id', Integer, primary_key = True)
	_hour24h = Column( 'Hour24H', Integer )
	_lightmode = Column( 'LightMode', Integer )
	_lightoff = Column( 'LightOff', Integer )
	_lighton = Column( 'LightOn', Integer )
	_tempmode = Column( 'TempMode', Integer )
	_tempoff = Column( 'TempOff', Integer )
	_tempon = Column( 'TempOn', Integer )
	_humimode = Column( 'HumiMode', Integer )
	_humioff = Column( 'HumiOff', Integer )
	_humion = Column( 'HumiOn', Integer )
	_dooronoff = Column( 'DoorOnOff', Integer )
	_shadeon = Column( 'ShadeOn', Integer )
	_uuid = Column( 'UUID', String(128) )

	def __init__(self, hour24, lmode, lon, loff, tmode, ton, toff, hmode, hon, hoff, door, shadeon, uuid):
		self._hour24 = hour24
		self._lightmode = lmode
		self._lighton = lon
		self._lightoff = loff
		self._tempmode = tmode
		self._tempon = ton
		self._tempoff = toff
		self._humimode = hmode
		self._humion = hon
		self._humioff = hoff
		self._dooronoff = door
		self._shadeon = shadeon
		self._uuid = uuid

class Light(Base):
	__tablename__ = 'Light'
#CREATE TABLE Light(id INTEGER PRIMARY KEY AUTOINCREMENT, light FLOAT, currentdate DATETIME DEFAULT (datetime(CURRENT_TIMESTAMP,'localtime')), device TEXT);
	_id = Column('id', Integer, primary_key = True)
	_light = Column('Light', Float)
	_curdate = Column('CurrentDate', DateTime(timezone=True), server_default=sql.func.now() )
	_uuid = Column('UUID', String(128))

	def __init__(self, Light, curdate = None, uuid = ''):
		self._light = Light
		self._curdate = curdate
		self._uuid = uuid

class Interval(Base):
	__tablename__ = 'Interval'
#CREATE TABLE Interval(id INTEGER PRIMARY KEY AUTOINCREMENT, `item` TEXT, seconds INTEGER);
	_id = Column('id', Integer, primary_key = True)
	_item = Column( 'Item', String(32) )
	_seconds = Column( 'Seconds', Integer )


class Settings(Base):
	__tablename__ = 'Settings'
#CREATE TABLE Settings(id INTEGER PRIMARY KEY AUTOINCREMENT, `item` TEXT, `setting` TEXT);
	_id = Column('id', Integer, primary_key = True)
	_item = Column( 'Item', String(32) )
	_setting = Column( 'Setting', String(32) )

class Humi(Base):
	__tablename__ = 'Humidity'
	_id = Column('id', Integer, primary_key = True)
	_humi = Column('Humidity', Float)
	_curdate = Column('CurrentDate', DateTime(timezone=True), server_default=sql.func.now() )
	_uuid = Column( 'UUID', String(128) )
#CREATE TABLE Humidity(id INTEGER PRIMARY KEY AUTOINCREMENT, humidity FLOAT, currentdate DATETIME DEFAULT (datetime(CURRENT_TIMESTAMP,'localtime')), device TEXT);

class Temp(Base):
	__tablename__ = 'Temperature'
#CREATE TABLE Temperature(id INTEGER PRIMARY KEY AUTOINCREMENT, temperature FLOAT, currentdate DATETIME DEFAULT (datetime(CURRENT_TIMESTAMP,'localtime')), device TEXT);
	_id = Column('id', Integer, primary_key = True)
	_temp = Column( 'Temperature', Float )
	_curdate = Column('CurrentDate', DateTime(timezone=True), server_default=sql.func.now() )
	_uuid = Column( 'UUID', String(128) )

	def __init__(self, temp, curTime, uuid = ''):
		self._temp = temp
		self._curdate = curTime
		self._uuid = uuid


class POnOff(Base):
	__tablename__ = 'POnOff'
#CREATE TABLE POnOff(id INTEGER PRIMARY KEY AUTOINCREMENT, Item TEXT, pStatus TEXT, currentdate DATETIME DEFAULT (datetime(CURRENT_TIMESTAMP,'localtime')), UUID TEXT);
	_id = Column('id', Integer, primary_key = True)
	_item = Column( 'Item', String(32) )
	_pStatus = Column( 'pStatus', String(32) )
	_curdate = Column('CurrentDate', DateTime(timezone=True), server_default=sql.func.now() )
	_uuid = Column( 'UUID', String(128) )


