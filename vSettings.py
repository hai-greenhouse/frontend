import time, re, json
import vDB
from clog import clog
import vSQLAlchemy


class vSettings():

	def __init__(self):
		self._dbh = vSQLAlchemy.vDB()

	#def updateSettings( self, hr = None, ln = None, lf = None, sn = None, tn = None, hn = None ):
		#print( " [vSettings.uSettings]:hr:ln:lf:sn:tn:hn ", hr, ln, lf, sn, tn, hn )
		#result = self._dbh.updateSettings( hr, ln, lf, sn, tn, hn )
		
		#return True
		
	def qInterval(self, mode = 'raw' ):
		datasource = self._dbh.getIntervals()

		if mode == 'raw':
			return datasource

		jraw = dict()
		jdata = []
		titem = dict()

		titem['execLight'] = "沒有作用"
		titem['execFan'] = "開啟風扇的秒數"
		titem['execHumi'] = "開啟加濕器的秒數"
		titem['cLight'] = "燈光的控制週期"
		titem['cTemp'] = "溫度的控制週期"
		titem['cHumi'] = "濕度的控制週期"
		titem['senLight'] = "紀錄燈光的週期"
		titem['senTemp'] = "記錄溫度的週期"
		titem['senHumi'] = "紀錄濕度的週期"
		titem['cDoor'] = "門的控制週期"

		if mode == 'json':
			for row in datasource:
				jds = { "id": row._id, 
						"item": row._item , 
						"seconds": row._seconds,
						"titem": titem[row._item] }
				jdata.append( jds )
			
			jraw['data'] = jdata
			return json.dumps( jraw )

	def qTimeOnOff(self, mode = 'raw' ):
		datasource = self._dbh.getTimeOnOffTable()

		if mode == 'raw':
			return datasource

		jraw = dict()
		jdata = []
		if mode == 'json':
			for row in datasource:
				""" jds = [ row._id, 
						row._hour24h , 
						row._lightoff,
						row._lighton,
						row._tempoff,
						row._tempon,
						row._humioff,
						row._humion,
						row._dooronoff ] """
				jds = { "id": row._id, 
						"hour24h": row._hour24h , 
						"lightoff": row._lightoff,
						"lighton": row._lighton,
						"tempoff": row._tempoff,
						"tempon": row._tempon,
						"humioff": row._humioff,
						"humion": row._humion,
						"shadeon": row._shadeon,
						"door": row._dooronoff }
				jdata.append( jds )
			
			jraw['data'] = jdata
			return json.dumps( jraw )
	
