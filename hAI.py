from flask import Flask, render_template, request, Response
#from pyecharts.globals import CurrentConfig

from pyecharts import options as opts
from websocket import create_connection

import time, datetime, json, threading, os, re, urllib
import vMainLine
import vSettings

#from camera import VideoCamera

# 关于 CurrentConfig，可参考 [基本使用-全局变量]
#CurrentConfig.GLOBAL_ENV = Environment(loader=FileSystemLoader("./templates"))

#pi_camera = VideoCamera( flip=False )

app = Flask(__name__, static_folder="templates")

def isnum( n ):
	try:
		r = float(n)
		return True
	except:
		return False

@app.route("/")
def index():
	return render_template( 'index.html' )

#@app.route("/video_feed")
#def video_feed():
	#return Response( gen(pi_camera),mimetype='multipart/x-mixed-replace; boundary=frame')

#def gen(camera):
	#while True:
		#frame = camera.get_frame()
		#yield (b'--frame\r\n'
			#b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')

@app.route("/SetFanPower/<Power>")
def SetFanPower( Power ):
	try:
		ws = create_connection("ws://127.0.0.1:34567")
		ws.send("ask,fanpower,"+Power)
		result = ws.recv()
		ws.close()
		return result
	except Exception as err:
		print( "WebSocket client SetFanPower err:", str(err) )
		return '0'

@app.route("/GetFanPower")
def GetFanPower():
	try:
		ws = create_connection("ws://127.0.0.1:34567")
		ws.send("query,fanpower")
		result = ws.recv()
		ws.close()
		return result
	except Exception as err:
		print( "WebSocket client GetFanPower err:", str(err) )
		return '0'

@app.route("/getTimeOnOff")
def jsonTimeOnOff():
	ST = vSettings.vSettings()
	l = ST.qTimeOnOff( 'json' )
	return l

@app.route("/getIntervals")
def jsonInterval():
	ST = vSettings.vSettings()
	l = ST.qInterval( 'json' )
	return l

@app.route("/setIntervals")
def setInterval():
	jdata = {}
	omsg = "變更後請按儲存"
	for item in ( 'execFan', 'execHumi', 'cLight', 'cTemp', 'cHumi', 'senLight', 'senTemp', 'senHumi', 'cDoor' ):
		temp = request.args.get( item )
		if isnum( temp ):
			jdata[ item ] = temp

	if len( jdata ) > 0:
		try:
			wstring = "in,interval,"+json.dumps(jdata)
			ws = create_connection("ws://127.0.0.1:34567")
			ws.send( wstring )
			result = ws.recv()
			if result == 'True':
				omsg = "儲存成功"
			ws.close()
		except Exception as err:
			omsg = 'WebSocket client inInterval Error, please try later'

	return render_template( 'Settings.html', msg = omsg )

@app.route("/Settings")
def pageSettings():

	"""
	dooronof = request.args.get('dooronoff')
	"""
	jdata = {}
	omsg = "變更後請按儲存"

	for item in ( 'hour24h', 'lighton', 'lightoff', 'shadeon', 'tempon', 'humion', 'dooronoff' ):
		temp = request.args.get( item )
		if isnum( temp ):
			jdata[ item ] = temp

	if len( jdata ) > 0:
		try:
			wstring = "in,timeonoff,"+json.dumps(jdata)
			ws = create_connection("ws://127.0.0.1:34567")
			ws.send( wstring )
			result = ws.recv()
			if result == 'True':
				omsg = "儲存成功"
			ws.close()
		except Exception as err:
			omsg = 'WebSocket client inTimeOnOff Error, please try later'

	return render_template( 'Settings.html', msg = omsg )

# Index ############################################################################## Index

@app.route("/getDeltaFirstDays")
def get_tMenu():
	ML = vMainLine.vMainLine()
	l = ML.getDeltaFirstDays()
	return l

@app.route("/LineChart")
def get_main_line():
	stime = request.args.get( 'stime', default = None, type = str )
	etime = request.args.get( 'etime', default = None, type = str )

	if stime is None or stime == 'false':
		unix_stime = None
	else:
		unix_stime = datetime.datetime.fromisoformat( "%s 00:00:00+08:00" % stime ).strftime( '%s' )

	if etime is None or etime == 'false':
		unix_etime = None
	else:
		unix_etime = datetime.datetime.fromisoformat( "%s 23:59:59+08:00" % etime ).strftime( '%s' )

	ML = vMainLine.vMainLine()
	l = ML.DrawMain( unix_stime, unix_etime )
	return l.dump_options_with_quotes()

@app.route("/LTH_Lumi")
def Board_Lumi():
	ML = vMainLine.vMainLine()
	l = ML.textLumi()
	return l

@app.route("/LTH_Humi")
def Board_Humi():
	ML = vMainLine.vMainLine()
	l = ML.DrawLTHBoard( mode=0 )
	return l.dump_options_with_quotes()

@app.route("/LTH_Temp")
def Board_Temp():
	ML = vMainLine.vMainLine()
	l = ML.DrawLTHBoard( mode=1 )
	return l.dump_options_with_quotes()

@app.route("/askLTHMode")
def askLTHMode():
	try:
		ws = create_connection("ws://127.0.0.1:34567")
		ws.send("ask,lthmode")
		result = ws.recv()
		ws.close()
		return result
	except Exception as err:
		print( "WebSocket client error:", str(err) )
		return '0'

@app.route("/askPowerStatus")
def askPowerStatus():
	try:
		ws = create_connection("ws://127.0.0.1:34567")
		ws.send("ask,powerstatus")
		result = ws.recv()
		ws.close()
		return result
	except Exception as err:
		print( "WebSocket client error:", str(err) )
		return '0'

@app.route("/PON/<OBJ>")
def PON( OBJ ):

	if OBJ is None:
		return '0'

	try:
		ws = create_connection("ws://127.0.0.1:34567")
	except Exception as err:
		print( "WebSocket client error:", str(err) )
		return '0'

	if OBJ == "Light":
		ws.send("ask,lightmanual")		# Turn On Manual

	if OBJ == "Temp":
		ws.send("ask,tempmanual")

	if OBJ == "Humi":
		ws.send("ask,humimanual")

	ws.close()
	return '1'

@app.route("/POFF/<OBJ>")
def POFF( OBJ ):

	if OBJ is None:
		return '0'

	try:
		ws = create_connection("ws://127.0.0.1:34567")
	except:
		print( "WebSocket client error" )
		return '0'

	if OBJ == "Light":
		ws.send("ask,lightauto")

	if OBJ == "Temp":
		ws.send("ask,tempauto")

	if OBJ == "Humi":
		ws.send("ask,humiauto")

	ws.close()
	return '1'

@app.route("/getPOnOff/<ITEM>/<QDATE>/")
@app.route("/getPOnOff/<ITEM>")
def getPOnOff( ITEM, QDATE = ''):

	if ITEM not in ('All', 'Light', 'Temperature', 'Humidity' ):
		return 'False'

	checkdate = re.match( '^\d\d\d\d\-\d?\d\-\d?\d$', QDATE )
	if not checkdate:
		QDATE = time.strftime( '%Y-%m-%d', time.localtime() )

	try:
		ws = create_connection("ws://127.0.0.1:34567")
		ws.send("query,ponoff,"+ITEM+","+QDATE)
		result = ws.recv()
		ws.close()
		return result
	except Exception as err:
		print( "WebSocket client:getPOnOff error:", str(err) )

	return '0'

@app.route("/getNote/<QDATE>")
@app.route("/getNote/")
def getNote( QDATE = ''):
	checkdate = re.match( '^\d\d\d\d\-\d?\d\-\d?\d$', QDATE )
	if not checkdate:
		QDATE = time.strftime( '%Y-%m-%d', time.localtime() )

	try:
		ws = create_connection("ws://127.0.0.1:34567")
		ws.send("query,note,"+QDATE)
		result = ws.recv()
		ws.close()
		return result
	except Exception as err:
		print( "WebSocket client:getNote error:", str(err) )

	return False

@app.route("/setNote/<qDate>/<uNote>")
def setNote( qDate, uNote ):

	checkdate = re.match( '^\d\d\d\d\-\d?\d\-\d?\d$', qDate )
	if not checkdate:
		QDATE = time.strftime( '%Y-%m-%d', time.localtime() )

	try:
		ws = create_connection("ws://127.0.0.1:34567")
		ws.send("in,note,"+qDate+","+urllib.parse.quote(uNote))
		result = ws.recv()
		ws.close()
		return result
	except Exception as err:
		print( "WebSocket client:setNote error:", str(err) )

	return False

if __name__ == "__main__":
	app.run(host="0.0.0.0")
