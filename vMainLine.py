import time, re
from pyecharts.charts import Line, Liquid, Gauge, Grid
from datetime import date
import pyecharts.options  as opts
from clog import clog
import vSQLAlchemy

class vMainLine():

    def __init__(self):
        self._pyline = Line()
        self._dbh = vSQLAlchemy.vDB()
        self._xpoint = 24
        self._lumi = []
        self._temp = []
        self._humi = []
        self._xaxis = []

    def getLumifromDB(self, stime = None, etime = None):

        if etime == None:
            etime = time.strftime( '%Y-%m-%d %H:%M:%S', time.localtime() )

        if stime == None:
            etime = time.strftime( '%Y-%m-%d %H:%M:%S', ( time.localtime() - 43200 ) )  # 12 hours ago

        result = self._dbh.getLight( stime, etime )

    def getAvgLumi( self, stime = None, etime = None ):
        return self._dbh.getLightAvg( stime, etime )[0]

    def getAvgTemp( self, stime = None, etime = None ):
        return self._dbh.getTempAvg( stime, etime )[0]

    def getAvgHumi( self, stime = None, etime = None ):
        return self._dbh.getHumiAvg( stime, etime )[0]


    def textLumi(self):
        return str( int( self._dbh.getLightNow() ) )

    def DrawLTHBoard(self, mode = 0, unix_stime = None, unix_etime = None):

        temp = self._dbh.getTempNow()
        humi = self._dbh.getHumiNow() 

        self._lHumi = Liquid()
        datahumi = ( int(humi._humi) / 100 )
        self._lHumi.add("濕度", [datahumi, (datahumi - 0.1) ] )
        #self._lHumi.set_global_opts( title_opts=opts.TitleOpts( title="目前度 %s" % humi[1] ) )

        self._gTemp = Gauge()
        self._gTemp.add( "", [("溫度", "{:3.1f}".format( temp._temp ) ) ], min_=0, max_=50, start_angle=180, end_angle=0 , detail_label_opts=opts.GaugeDetailOpts( formatter="\n\n\n{value} 度", color="black" ) )
        #self._gTemp.set_global_opts( title_opts=opts.TitleOpts( title="目前溫度 %s" % humi[1] ) )

        #self._gridTH = Grid()
        #self._gridTH.add( self._gTemp, grid_opts=opts.GridOpts( pos_left="left" ), grid_index=0 )
        #self._gridTH.add( self._lHumi, grid_opts=opts.GridOpts( pos_left="right" ), grid_index=1 )

        if mode == 0:
            return self._lHumi
        else:
            return self._gTemp

        #    return self._gridTH

    def getDeltaFirstDays(self):

        result = self._dbh.getLightOrder( order = 'ASC' )
        x = re.search( '(\d\d\d\d)\-(\d\d)-(\d\d) ', str( result[0]._curdate ) )
        if x.group(1):
            bdate = date( int(x.group(1)), int(x.group(2)), int(x.group(3)) )
            ndate = date.today()
            delta = bdate - ndate
            return str(delta.days)

        return 0

    def DrawMain(self, unix_stime = None, unix_etime = None):
        # stime and etime is unix timestamp

        self._lumi = []
        self._temp = []
        self._humi = []
        self._xaxis = []


        if unix_stime == None:
            stime = time.time() - 86400
        else:
            stime = int(unix_stime)

        title_day = "hAI 數據統計 (" +  time.strftime( '%Y-%m-%d %H 時', time.localtime(stime) ) + ")"

        if unix_etime == None:
            etime = stime + 86400       # 23 hours
            title_day += " + 23 小時"
        else:
            etime = int(unix_etime)
            title_day += " - (" +  time.strftime( '%Y-%m-%d', time.localtime(etime) ) + ")"

        #POnOffArea = []
        
        timestep = ( etime - stime ) / self._xpoint
        for atime in range( int(stime), int(etime), int(timestep) ):
            t1 = time.strftime( '%Y-%m-%d %H:00:00', time.localtime( atime ) )
            t2 = time.strftime( '%Y-%m-%d %H:59:59', time.localtime( atime ) )
            if timestep > 3600: # > 1 day
                HH = time.strftime( '%y-%m-%d %H', time.localtime( atime ) )
            else:
                HH = time.strftime( '%H', time.localtime( atime ) )

            self._xaxis.append( HH )

            data = int( self.getAvgLumi( t1, t2 ) )
            if data <= 0:
                data = 1      # Log can't show log(0)
            self._lumi.append( data )

            data = int( self.getAvgTemp( t1, t2 ) )
            if data <= 0:
                data = 1      # Log can't show log(0)
            self._temp.append( data )

            data = int( self.getAvgHumi( t1, t2 ) )
            if data <= 0:
                data = 1      # Log can't show log(0)
            self._humi.append( data )


        cLine = { "Lumi":"#ff8c00", "Temp":"#B22222", "Humi":"#0000CD" }
        self.mline = Line( init_opts=opts.InitOpts(width="1000px", height="600px") )
        self.mline.add_xaxis( self._xaxis )
        self.mline.add_yaxis( series_name='亮度', y_axis=self._lumi, is_smooth=True, color=cLine['Lumi'])
        self.mline.add_yaxis( series_name='溫度', y_axis=self._temp, is_smooth=True, markpoint_opts=opts.MarkPointOpts(
                                                                                            data=[
                                                                                                opts.MarkPointItem(type_="max", name="最高溫"),
                                                                                                opts.MarkPointItem(type_="min", name="最低溫")
                                                                                                ] ),
                                                                  markline_opts=opts.MarkLineOpts(
                                                                                            data=[
                                                                                                opts.MarkLineItem(type_="max", name="最高溫"),
                                                                                                opts.MarkLineItem(type_="min", name="最低溫")
                                                                                                ] ),
                                                                  color=cLine['Temp'])
        self.mline.add_yaxis( series_name='濕度', y_axis=self._humi, is_smooth=True, color=cLine['Humi'])

        self.mline.set_global_opts(title_opts=opts.TitleOpts( title=title_day ),
                                    xaxis_opts=opts.AxisOpts( name="時" ),
                                    yaxis_opts=opts.AxisOpts( type_="log",
                                                                splitline_opts=opts.SplitLineOpts(is_show=True),
                                                                is_scale=True,
                                                                ) )

        #self.mline.set_series_opts( markarea_opts=opts.MarkAreaOpts(
                                    #data=[ POnOffArea ]
                                    #) )


        # bug: https://blog.csdn.net/u011888840/article/details/105700119
        num = 0
        for color in cLine.values():
            self.mline.colors[num] = color
            num += 1

        return self.mline


