import re
import time

def clog( msg1, msg2 = None, msg3 = None, msg4 = None, msg5 = None , msg6 = None, msg7 = None, msg8 = None ):
    msgall = ''

    for msg in [msg1, msg2, msg3, msg4, msg5, msg6, msg7, msg8]:
        if msg is not None:
            msgall = msgall + str(msg)

    filename = time.strftime( '%Y-%m-%d.log', time.localtime() )
    filename = "/var/www/html/v-hAI/log/%s" % filename

    try:
        f = open( filename, "a" )
        f.write( time.strftime( '%H:%M:%S', time.localtime() ) )
        f.write( msgall )
        f.write( "\n" )
        f.close()
    except Exception as e:
        print( " Error!!:", str(e) )

    if re.search( r'^\[Error\]', msgall):
        print( time.strftime('%H:%M', time.localtime() ), msgall )

